<?php if($message = Session::get('success')): ?>
    <div class="alert success-alert">
        <p><?php echo e($message); ?></p>
    </div>
<?php endif; ?>
<?php if($message = Session::get('danger')): ?>
    <div class="alert danger-alert">
        <p><?php echo e($message); ?></p>
    </div>
<?php endif; ?>
<?php /**PATH C:\OSPanel\domains\music-master\resources\views/layouts/message.blade.php ENDPATH**/ ?>