<section id="catalog" class="catalog">
    <div class="catalog__container container">
        <div class="catalog__title">
            Наш каталог пластинок
        </div>

        <form class="catalog__genres-form" action="#!" method="GET">
            <div class="catalog__genres">
                <button class="catalog__genres-button" type="button" name="genre" value="all">Все</button>
                <button class="catalog__genres-button" type="button" name="genre" value="<?php echo e(\App\Enums\GenreEnum::HIP_HOP->value); ?>">Hip hop</button>
                <button class="catalog__genres-button" type="button" name="genre" value="lo-fi"><?php echo e(\App\Enums\GenreEnum::LOFI->value); ?></button>
                <button class="catalog__genres-button" type="button" name="genre" value="lounge"><?php echo e(\App\Enums\GenreEnum::LOUNGE->value); ?></button>
                <button class="catalog__genres-button" type="button" name="genre" value="jazz"><?php echo e(\App\Enums\GenreEnum::JAZZ->value); ?></button>
                <button class="catalog__genres-button" type="button" name="genre" value="ambient"><?php echo e(\App\Enums\GenreEnum::AMBIENT->value); ?></button>
            </div>
            <input id="image_src" class="image__src" type="text" hidden name="image_src" value="<?php echo e(asset('storage/')); ?>">
        </form>
        <div class="catalog__items">

        </div>
        <form id="show-more-form" action="<?php echo e(url()->current()); ?>" method="get" class="show-more__wrapper">
            <input type="hidden" name="show-more" value="true">
            <button id="show-more-btn" class="button-orange catalog__show-more">Показать ещё</button>
            <?php if(request()->has('genre')): ?>
                <input type="hidden" name="genre" value="<?php echo e(request()->input('genre')); ?>">
            <?php endif; ?>
            <input id="image_src" class="image__src" type="text" hidden name="image_src" value="<?php echo e(asset('storage/')); ?>">
        </form>
    </div>
</section>
<?php /**PATH C:\OSPanel\domains\music-master\resources\views/components/catalog.blade.php ENDPATH**/ ?>