<?php $__env->startSection('content'); ?>
    <div class="page-body">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header pb-0">
                            <h4>Редактирование артиста</h4>
                        </div>
                        <form method="post" action="<?php echo e(route('admin.artist.update', ['id' => $artist->id])); ?>" enctype="multipart/form-data" class="form theme-form">
                            <?php echo method_field('PATCH'); ?>
                            <?php echo csrf_field(); ?>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <div class="mb-3">
                                            <label class="form-label" for="exampleFormControlInput1"> Имя артиста</label>
                                            <input class="form-control" id="exampleFormControlInput1" value="<?php echo e($artist->name); ?>" name="name" type="text" placeholder="Тимати">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <div class="mb-3">
                                            <label class="form-label" for="exampleInputPassword2">Описание</label>
                                            <textarea class="form-control" name="description"><?php echo e($artist->description); ?></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <div class="mb-3">
                                            <label class="form-label" for="exampleInputPassword4">Фотография артиста</label>
                                            <input class="form-control" name="image_url" id="exampleInputPassword4" type="file">
                                            <a href="">Изображение артиста</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer text-end">
                                <button class="btn btn-primary" type="submit">Редактировать артиста</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\OSPanel\domains\music-master\resources\views/admin/artist/edit.blade.php ENDPATH**/ ?>