<?php $__env->startSection('content'); ?>
    <div class="page-body">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header pb-0">
                            <h4>Ответ на вопрос</h4>
                            <br>
                            <p> Вопрос:
                                <?php echo e($feedback->description); ?>

                            </p>
                        </div>
                        <form method="post" action="<?php echo e(route('admin.feedback.answer', ['id' => $feedback->id])); ?>" class="form theme-form">
                            <?php echo csrf_field(); ?>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <div class="mb-3">
                                            <label class="form-label" for="exampleFormControlInput1">Ответ на вопрос</label>
                                            <textarea class="form-control" placeholder="Описание о нас" name="message" cols="30" rows="30">
                                            </textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer text-end">
                                <button class="btn btn-primary" type="submit">Отправить</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\OSPanel\domains\music-master\resources\views/admin/feedback/edit.blade.php ENDPATH**/ ?>