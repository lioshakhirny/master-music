<div class="block-container-tovar">
    <div class="block-img">
        <div class="record-container">
            <div class="records" id="record">
                <img src=" <?php echo e(asset('images/plastinka.svg')); ?>" alt="Record" class="record-img">
            </div>
        </div>
        <img src="<?php echo e(asset('images/IzPlast2.png')); ?>" alt="Background" class="background-img">
    </div>
    <div class="block-txt">
        <h2 class="name"><?php echo e($record->name); ?></h2>
        <div class="block-soderhanie">
            <div class="soderhanie-A">
                <strong>SIDE A</strong>
                <?php $__currentLoopData = $musics; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $music): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <p style="cursor:pointer;" class="sideA"><?php echo e($music->side_a); ?></p>
                    <input type="hidden" class="side_a" value="<?php echo e(\Illuminate\Support\Facades\Storage::url('public/files/music/sideA/' . $music->record_id . '/' . $music->side_a)); ?>">
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </div>
            <div class="soderhanie-B">
                <strong>SIDE B</strong>
                <?php $__currentLoopData = $musics; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $music): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <p style="cursor:pointer;" class="sideB"><?php echo e($music->side_b); ?></p>
                    <input type="hidden" class="side_b" value="<?php echo e(\Illuminate\Support\Facades\Storage::url('public/files/music/sideB/' . $music->record_id . '/' . $music->side_b)); ?>">
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </div>
        </div>
        <div class="dop-opisanie">
            <span>Жанр:<?php echo e(\App\Models\Genre::find($record->genre_id)->title); ?></span>
            <span>Год издания:<?php echo e($record->created_at); ?></span>
            <span>Альбом: <?php echo e(\App\Models\Album::find($record->album_id)->name); ?></span>
        </div>
        <div class="prise">
            <h4>Цена:</h4>
            <p><?php echo e($record->price - ($record->price * ($record->discount/100))); ?> BYN</p>
        </div>
        <button class="bue">Купить пластинку</button>
    </div>
</div>
<div class="block-perehod">
    <audio class="aud" controls src="" id="audio"></audio>
    <div class="block-perehod-opisanie">
        <p><?php echo e($record->description); ?></p>
    </div>
</div>
<script>
    const audio = document.getElementById("audio");
    const record = document.getElementById("record");
    const sideA = document.querySelectorAll('.sideA');
    const audioA = document.querySelectorAll('.side_a');

    const sideB = document.querySelectorAll('.sideB');
    const audioB = document.querySelectorAll('.side_b');

    for (let i=0; i<sideA.length; i++) {
        sideA[i].addEventListener('click', function () {
            audio.src = audioA[i].value
        });
    }

    for (let i=0; i<sideB.length; i++) {
        sideB[i].addEventListener('click', function () {
            audio.src = audioB[i].value
        });
    }

    audio.onplay = function() {
        record.classList.add('rekk'); // Добавляем класс, чтобы начать анимацию вращения
    };

    audio.onpause = function() {
        record.classList.remove('rekk'); // Убираем класс, чтобы остановить анимацию вращения
    };

</script>
<?php /**PATH C:\OSPanel\domains\music-master\resources\views/layouts/BlockCartochka.blade.php ENDPATH**/ ?>