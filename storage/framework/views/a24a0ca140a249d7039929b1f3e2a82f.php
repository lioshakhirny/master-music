<?php $__env->startSection('content'); ?>
    <div class="page-body">
        <div class="container-fluid">
            <div class="page-title">
                <div class="row">
                    <div class="col-sm-6">
                        <h3>Артисты</h3>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?php echo e(route('admin.index')); ?>"><i data-feather="home"></i></a></li>
                            <li class="breadcrumb-item">Артисты</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <!-- Container-fluid starts-->
        <div class="container-fluid basic_table">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h4>Список  артистов</h4>
                            <br>
                            <a href="<?php echo e(route('admin.artist.create')); ?>" class="btn btn-primary">Добавить артиста</a>
                        </div>
                        <div class="table-responsive theme-scrollbar">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th scope="col">Id</th>
                                    <th scope="col">Имя</th>
                                    <th scope="col">Описание</th>
                                    <th scope="col">Изображение</th>
                                    <th scope="col">Действия</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $__currentLoopData = $artists; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $artist): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <tr>
                                        <th scope="row"><?php echo e($artist->id); ?></th>
                                        <td class=""><?php echo e($artist->name); ?></td>
                                        <td class=""><?php echo e($artist->description); ?></td>
                                        <td class=""><img src="<?php echo e($artist->image_url); ?>" style="border-radius: 50%; width: 40px; height: 40px"></td>
                                        <td>
                                            <div class="row">
                                                <a href="<?php echo e(route('admin.artist.edit', ['id' => $artist->id])); ?>">Редактировать</a>
                                                <form action="<?php echo e(route('admin.artist.destroy', ['id' => $artist->id])); ?>" method="post">
                                                    <?php echo method_field('DELETE'); ?>
                                                    <?php echo csrf_field(); ?>
                                                    <button type="submit" class="btn btn-primary">Удалить</button>
                                                </form>
                                            </div>
                                        </td>
                                    </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Container-fluid Ends-->
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\OSPanel\domains\music-master\resources\views/admin/artist/index.blade.php ENDPATH**/ ?>