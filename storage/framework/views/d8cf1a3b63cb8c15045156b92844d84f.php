<?php $__env->startSection('content'); ?>
    <div class="page-body">
        <div class="container-fluid">
            <div class="page-title">
                <div class="row">
                    <div class="col-sm-6">
                        <h3>Музыка</h3>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?php echo e(route('admin.index')); ?>"><i data-feather="home"></i></a></li>
                            <li class="breadcrumb-item">Музыка</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <!-- Container-fluid starts-->
        <div class="container-fluid basic_table">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h4>Список музыки</h4>
                            <br>
                            <a href="<?php echo e(route('admin.record-music.create')); ?>" class="btn btn-primary">Добавить музыку</a>
                        </div>
                        <div class="table-responsive theme-scrollbar">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th scope="col">Id</th>
                                    <th scope="col">Сторона А</th>
                                    <th scope="col">Сторона В</th>
                                    <th scope="col">Пластинка</th>
                                    <th scope="col">Действия</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $__currentLoopData = $recordMusics; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $recordMusic): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <tr>
                                        <th scope="row"><?php echo e($recordMusic->id); ?></th>
                                        <td class=""><?php echo e($recordMusic->side_a); ?></td>
                                        <td class=""><?php echo e($recordMusic->side_b); ?></td>
                                        <td class=""><?php echo e(\App\Models\Record::find($recordMusic->record_id)->name); ?></td>
                                        <td>
                                            <div class="row">
                                                <a href="<?php echo e(route('admin.record-music.edit', ['id' => $recordMusic->id])); ?>">Редактировать</a>
                                                <form
                                                    action="<?php echo e(route('admin.record-music.destroy', ['id' => $recordMusic->id])); ?>"
                                                    method="post">
                                                    <?php echo method_field('DELETE'); ?>
                                                    <?php echo csrf_field(); ?>
                                                    <button type="submit" class="btn btn-primary">Удалить</button>
                                                </form>
                                            </div>
                                        </td>
                                    </tr>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\OSPanel\domains\music-master\resources\views/admin/record/music/index.blade.php ENDPATH**/ ?>