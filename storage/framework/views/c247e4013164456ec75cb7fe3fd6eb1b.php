<?php $__env->startSection('content'); ?>
    <div class="page-body">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header pb-0">
                        <h4>Создание  жанра</h4>
                    </div>
                    <form method="post" action="<?php echo e(route('admin.genre.store')); ?>" enctype="multipart/form-data" class="form theme-form">
                        <?php echo csrf_field(); ?>
                        <div class="card-body">
                            <div class="row">
                                <div class="col">
                                    <div class="mb-3">
                                        <label class="form-label" for="exampleFormControlInput1"> Название</label>
                                        <input class="form-control" id="exampleFormControlInput1" name="name" type="text" placeholder="Тимати">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="mb-3">
                                        <label class="form-label" for="exampleFormControlInput1">Слаг</label>
                                        <input class="form-control" id="exampleFormControlInput1" name="slug" type="text" placeholder="Rock">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer text-end">
                            <button class="btn btn-primary" type="submit">Добавить  жанр</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\OSPanel\domains\music-master\resources\views/admin/genre/create.blade.php ENDPATH**/ ?>