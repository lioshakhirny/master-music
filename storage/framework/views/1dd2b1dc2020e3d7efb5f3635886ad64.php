<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo e(asset('dist/css/app/app.css')); ?>">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper@11/swiper-bundle.min.css"/>
    <script src="https://cdn.jsdelivr.net/npm/swiper@11/swiper-bundle.min.js"></script>
    <script src="<?php echo e(asset('dist/js/app/app.js')); ?>"></script>
    <title>Пластинка</title>
</head>
<body>
    <div class="hero__block-wrapper">
        <?php echo $__env->make('layouts.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
        <?php echo $__env->make('layouts.hero', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    </div>
    <?php echo $__env->make('layouts.info', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <?php if (isset($component)) { $__componentOriginal688a37c2a74e2e61290753be22cd0929 = $component; } ?>
<?php if (isset($attributes)) { $__attributesOriginal688a37c2a74e2e61290753be22cd0929 = $attributes; } ?>
<?php $component = App\View\Components\Catalog::resolve([] + (isset($attributes) && $attributes instanceof Illuminate\View\ComponentAttributeBag ? (array) $attributes->getIterator() : [])); ?>
<?php $component->withName('catalog'); ?>
<?php if ($component->shouldRender()): ?>
<?php $__env->startComponent($component->resolveView(), $component->data()); ?>
<?php if (isset($attributes) && $attributes instanceof Illuminate\View\ComponentAttributeBag && $constructor = (new ReflectionClass(App\View\Components\Catalog::class))->getConstructor()): ?>
<?php $attributes = $attributes->except(collect($constructor->getParameters())->map->getName()->all()); ?>
<?php endif; ?>
<?php $component->withAttributes([]); ?>
<?php echo $__env->renderComponent(); ?>
<?php endif; ?>
<?php if (isset($__attributesOriginal688a37c2a74e2e61290753be22cd0929)): ?>
<?php $attributes = $__attributesOriginal688a37c2a74e2e61290753be22cd0929; ?>
<?php unset($__attributesOriginal688a37c2a74e2e61290753be22cd0929); ?>
<?php endif; ?>
<?php if (isset($__componentOriginal688a37c2a74e2e61290753be22cd0929)): ?>
<?php $component = $__componentOriginal688a37c2a74e2e61290753be22cd0929; ?>
<?php unset($__componentOriginal688a37c2a74e2e61290753be22cd0929); ?>
<?php endif; ?>
    <?php if (isset($component)) { $__componentOriginal59dcbb29135115974a5468b3e1c2e4e8 = $component; } ?>
<?php if (isset($attributes)) { $__attributesOriginal59dcbb29135115974a5468b3e1c2e4e8 = $attributes; } ?>
<?php $component = App\View\Components\Record::resolve([] + (isset($attributes) && $attributes instanceof Illuminate\View\ComponentAttributeBag ? (array) $attributes->getIterator() : [])); ?>
<?php $component->withName('record'); ?>
<?php if ($component->shouldRender()): ?>
<?php $__env->startComponent($component->resolveView(), $component->data()); ?>
<?php if (isset($attributes) && $attributes instanceof Illuminate\View\ComponentAttributeBag && $constructor = (new ReflectionClass(App\View\Components\Record::class))->getConstructor()): ?>
<?php $attributes = $attributes->except(collect($constructor->getParameters())->map->getName()->all()); ?>
<?php endif; ?>
<?php $component->withAttributes([]); ?>
<?php echo $__env->renderComponent(); ?>
<?php endif; ?>
<?php if (isset($__attributesOriginal59dcbb29135115974a5468b3e1c2e4e8)): ?>
<?php $attributes = $__attributesOriginal59dcbb29135115974a5468b3e1c2e4e8; ?>
<?php unset($__attributesOriginal59dcbb29135115974a5468b3e1c2e4e8); ?>
<?php endif; ?>
<?php if (isset($__componentOriginal59dcbb29135115974a5468b3e1c2e4e8)): ?>
<?php $component = $__componentOriginal59dcbb29135115974a5468b3e1c2e4e8; ?>
<?php unset($__componentOriginal59dcbb29135115974a5468b3e1c2e4e8); ?>
<?php endif; ?>
    <?php echo $__env->make('layouts.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
</body>
</html>
<?php /**PATH C:\OSPanel\domains\music-master\resources\views/main/main.blade.php ENDPATH**/ ?>