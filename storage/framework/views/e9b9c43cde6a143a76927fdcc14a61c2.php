<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo e(asset('dist/css/app/app.css')); ?>">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper@11/swiper-bundle.min.css"/>
    <script src="https://cdn.jsdelivr.net/npm/swiper@11/swiper-bundle.min.js"></script>
    <script src="<?php echo e(asset('dist/js/app/app.js')); ?>"></script>
    <title>Пластинка</title>
</head>
<body style="background: rgb(165, 180, 247)">

<div class="head__block-wrapper">
    <?php echo $__env->make('layouts.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
</div>
<div class="container__categories">
    <?php echo $__env->make('layouts.poiskHanr', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <div class="block-tovar" style="display: flex; justify-content: space-around; flex-wrap: wrap">
        <?php $__currentLoopData = $records; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $record): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class="catalog__itemm">
                <div class="catalog__item-img">
                    <img
                        src="https://im.kommersant.ru/Issues.photo/NEWS/2023/02/27/KMO_191711_00091_1_t222_155015.jpg"
                        alt="author">
                </div>
                <div class="catalog__item-content catalog__content">
                    <span class="catalog__content-author"><?php echo e(\App\Models\Artist::find($record->artist_id)->name); ?></span>
                    <h4 class="catalog__content-title"><?php echo e($record->name); ?></h4>
                    <div class="catalog__content-wrapper">
                        <div class="catalog__content-genres">
                            <div class="<?php echo e(\App\Enums\GenreEnum::tryFrom(\App\Models\Genre::find($record->genre_id)->title)?->color()); ?>">
                                <?php echo e(\App\Models\Genre::find($record->genre_id)->title); ?>

                            </div>
                        </div>
                        <div class="catalog__content-cost catalog-cost">
                            <span class="discount">-<?php echo e($record->discount); ?>%</span>
                            <div class="catalog-cost-discount"><?php echo e($record->price - ($record->price * ($record->discount/100))); ?>BYN</div>
                            <div class="catalog-cost-original"><?php echo e($record->price); ?> BYN</div>
                        </div>
                    </div>
                    <a href="<?php echo e(route('catalog.show', ['id' => $record->id])); ?>">Подробнее</a>
                </div>
            </div>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </div>
</div>
<?php echo $__env->make('layouts.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
</body>
</html>
<?php /**PATH C:\OSPanel\domains\music-master\resources\views/catalog/catalog.blade.php ENDPATH**/ ?>