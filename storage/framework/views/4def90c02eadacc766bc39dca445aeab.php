<?php $__env->startSection('content'); ?>
    <div class="page-body">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header pb-0">
                            <h4>Редактирование жанра</h4>
                        </div>
                        <form method="post" action="<?php echo e(route('admin.genre.update', ['id' => $genre->id])); ?>" enctype="multipart/form-data" class="form theme-form">
                            <?php echo method_field('PATCH'); ?>
                            <?php echo csrf_field(); ?>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <div class="mb-3">
                                            <label class="form-label" for="exampleFormControlInput1"> Название</label>
                                            <input class="form-control" id="exampleFormControlInput1" name="title" value="<?php echo e($genre->title); ?>" type="text" placeholder="Тимати">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <div class="mb-3">
                                            <label class="form-label" for="exampleFormControlInput1">Слаг</label>
                                            <input class="form-control" id="exampleFormControlInput1" value="<?php echo e($genre->slug); ?>" name="slug" type="text" placeholder="Rock">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer text-end">
                                <button class="btn btn-primary" type="submit">Редактировать жанр</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.admin.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\OSPanel\domains\music-master\resources\views/admin/genre/edit.blade.php ENDPATH**/ ?>