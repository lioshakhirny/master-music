<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Album;
use App\Models\Artist;
use App\Models\Genre;
use App\Models\Record;
use Illuminate\Http\Request;

class RecordController extends Controller
{
    public function index (): \Illuminate\Contracts\View\View|\Illuminate\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\Foundation\Application
    {
        return view('admin.record.index', [
            'records' => Record::all()
        ]);
    }

    public function create (): \Illuminate\Contracts\View\View|\Illuminate\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\Foundation\Application
    {
        return view('admin.record.create', [
            'albums' => Album::all(),
            'genres' => Genre::all(),
            'artists' => Artist::all()
        ]);
    }

    public function store ()
    {

    }

    public function edit ()
    {

    }

    public function update ()
    {

    }

    public function delete ()
    {

    }
}
