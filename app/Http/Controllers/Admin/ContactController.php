<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Contact\UpdateRequest;
use App\Models\Contact;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    public function index ()
    {
        return view('admin.contact.index', [
            'contact' => Contact::find(1)
        ]);
    }

     public function update (UpdateRequest $request)
     {
         Contact::query()->updateOrCreate([
             'id' => 1
         ],
             [
                 'address' => $request->address,
                 'phone' => $request->phone,
                 'email' => $request->email,
                 'work' => $request->work
             ]
         );

         return redirect()->back()->with('success', 'Контакты успешно обновлены');
     }
}
