<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Feedback;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class FeedbackController extends Controller
{
    public function index ()
    {
        return view('admin.feedback.index', [
            'feedbacks' => Feedback::all()
        ]);
    }

    public function edit (Feedback $id) {
        return view('admin.feedback.edit', [
            'feedback' => $id
        ]);
    }

    public function answer (int $id, Request $request) {
        Mail::send('admin.verification.question', ['question' => Feedback::find($id)->description,
            'otvet' => $request->message], function ($message) use ($id) {
            $message->to(Feedback::find($id)->email, 'Домашний уют')->subject
            ('Домашний уют');
            $message->from('lioshakhirny@gmail.com', 'Домашний уют');
        });

        Feedback::query()->find($id)->delete();

        return redirect()->route('admin.feedback.index')->with('Ответ отправлен на почту');
    }
}
