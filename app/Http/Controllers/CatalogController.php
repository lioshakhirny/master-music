<?php

namespace App\Http\Controllers;

use App\Models\Record;
use App\Models\RecordMusic;
use Illuminate\Http\Request;

class CatalogController extends Controller
{
    public function index ()
    {
        return view('catalog.catalog', [
            'records' => Record::all()
        ]);
    }

    public function show (Record $id)
    {
        return view('cartochka.cartochka', [
            'record' => $id,
            'musics' => RecordMusic::query()->where('record_id', '=', $id->id)->get()
        ]);
    }
}
