@extends('layouts.admin.main')
@section('content')
    <div class="page-body">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header pb-0">
                        <h4>Создание альбома</h4>
                    </div>
                    <form method="post" action="{{route('admin.album.store')}}" enctype="multipart/form-data" class="form theme-form">
                        @csrf
                        <div class="card-body">
                            <div class="row">
                                <div class="col">
                                    <div class="mb-3">
                                        <label class="form-label" for="exampleFormControlInput1"> Название</label>
                                        <input class="form-control" id="exampleFormControlInput1" name="name" type="text" placeholder="Тимати">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="mb-3">
                                        <label class="form-label" for="exampleFormControlInput1">Артист</label>
                                         <select class="form-control" name="artist_id">
                                             @foreach($artists as $artist)
                                                 <option value="{{$artist->id}}">{{$artist->name}}</option>
                                             @endforeach
                                         </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer text-end">
                            <button class="btn btn-primary" type="submit">Добавить  альбом</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection
