@extends('layouts.admin.main')
@section('content')
    <div class="page-body">
        <div class="container-fluid">
            <div class="page-title">
                <div class="row">
                    <div class="col-sm-6">
                        <h3>Артисты</h3>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ route('admin.index') }}"><i data-feather="home"></i></a></li>
                            <li class="breadcrumb-item">Артисты</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <!-- Container-fluid starts-->
        <div class="container-fluid basic_table">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            <h4>Список  артистов</h4>
                            <br>
                            <a href="{{route('admin.artist.create')}}" class="btn btn-primary">Добавить артиста</a>
                        </div>
                        <div class="table-responsive theme-scrollbar">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th scope="col">Id</th>
                                    <th scope="col">Имя</th>
                                    <th scope="col">Описание</th>
                                    <th scope="col">Изображение</th>
                                    <th scope="col">Действия</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($artists as $artist)
                                    <tr>
                                        <th scope="row">{{$artist->id}}</th>
                                        <td class="">{{$artist->name}}</td>
                                        <td class="">{{$artist->description}}</td>
                                        <td class=""><img src="{{$artist->image_url}}" style="border-radius: 50%; width: 40px; height: 40px"></td>
                                        <td>
                                            <div class="row">
                                                <a href="{{route('admin.artist.edit', ['id' => $artist->id])}}">Редактировать</a>
                                                <form action="{{route('admin.artist.destroy', ['id' => $artist->id])}}" method="post">
                                                    @method('DELETE')
                                                    @csrf
                                                    <button type="submit" class="btn btn-primary">Удалить</button>
                                                </form>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Container-fluid Ends-->
    </div>
@endsection
