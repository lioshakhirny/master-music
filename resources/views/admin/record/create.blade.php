@extends('layouts.admin.main')
@section('content')
    <div class="page-body">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header pb-0">
                            <h4>Создание пластинок</h4>
                        </div>
                        <form method="post" action="{{route('admin.record.store')}}" enctype="multipart/form-data" class="form theme-form">
                            @csrf
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <div class="mb-3">
                                            <label class="form-label" for="exampleFormControlInput1"> Название</label>
                                            <input class="form-control" id="exampleFormControlInput1" name="name" type="text" placeholder="Тимати">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <div class="mb-3">
                                            <label class="form-label" for="exampleInputPassword2">Описание</label>
                                            <textarea class="form-control" name="description"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <div class="mb-3">
                                            <label class="form-label" for="exampleInputPassword4">Цена</label>
                                            <input class="form-control" name="price" id="exampleInputPassword4" type="number">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <div class="mb-3">
                                            <label class="form-label" for="exampleInputPassword4">Скидка</label>
                                            <input class="form-control" name="discount" id="exampleInputPassword4" type="number">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <div class="mb-3">
                                            <label class="form-label" for="exampleFormControlInput1">Артист</label>
                                            <select class="form-control" name="artist_id">
                                                @foreach($artists as $artist)
                                                    <option value="{{$artist->id}}">{{$artist->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <div class="mb-3">
                                            <label class="form-label" for="exampleFormControlInput1">Жанр</label>
                                            <select class="form-control" name="genre_id">
                                                @foreach($genres as $genre)
                                                    <option value="{{$genre->id}}">{{$genre->title}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <div class="mb-3">
                                            <label class="form-label" for="exampleFormControlInput1">Альбом</label>
                                            <select class="form-control" name="album_id">
                                                @foreach($albums as $album)
                                                    <option value="{{$album->id}}">{{$album->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <div class="mb-3">
                                            <label class="form-label" for="exampleInputPassword4">Фотография пластинки</label>
                                            <input class="form-control" name="image_url" id="exampleInputPassword4" type="file">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer text-end">
                                <button class="btn btn-primary" type="submit">Добавить пластинку</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
