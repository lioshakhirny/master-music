@extends('layouts.admin.main')
@section('content')
    <div class="page-body">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header pb-0">
                            <h4>Контакты</h4>
                        </div>
                        <form method="post" action="{{route('admin.contact.update')}}" class="form theme-form">
                            @method('PATCH')
                            @csrf
                            <div class="card-body">
                                <div class="row">
                                    <div class="col">
                                        <div class="mb-3">
                                            <label class="form-label" for="exampleFormControlInput1">Адрес</label>
                                            <input class="form-control" id="exampleFormControlInput1" value="{{$contact->address ?? null}}" name="address" type="text" placeholder="">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <div class="mb-3">
                                            <label class="form-label" for="exampleFormControlInput1">Телефон</label>
                                            <input class="form-control" id="exampleFormControlInput1" value="{{$contact->phone ?? null}}" name="phone" type="tel" placeholder="Тимати">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <div class="mb-3">
                                            <label class="form-label" for="exampleFormControlInput1">Почта</label>
                                            <input class="form-control" id="exampleFormControlInput1" value="{{$contact->email ?? null}}" name="email" type="email" placeholder="Тимати">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <div class="mb-3">
                                            <label class="form-label" for="exampleFormControlInput1">Рабочие часы</label>
                                            <input class="form-control" id="exampleFormControlInput1" value="{{$contact->work ?? null}}" name="work" type="text" placeholder="Тимати">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer text-end">
                                <button class="btn btn-primary" type="submit">Редактировать</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
